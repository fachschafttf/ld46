﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressController : MonoBehaviour
{
    [Range(0f, 1f)]
    public float zeppelinProgress = 0;
    [Range(0f, 1f)]
    public float skullProgress = 0;

    private RectTransform myTransform;
    private GameObject zeppelinPanel;
    private RectTransform zeppelinTRansform;
    private GameObject skullPanel;
    private Image skullImage;

    private ShipManager ship;

    public float speedScaling = 10;
    public float ScullThreshold = 0.1f;
    public float ScullSpeed = 10;
    public bool scull = false;

    public float threadDistance = 0f;

    // Start is called before the first frame update
    void Start()
    {
        myTransform = GetComponent<RectTransform>();
        zeppelinPanel = transform.Find("Zeppelin").gameObject;
        zeppelinTRansform = zeppelinPanel.GetComponent<RectTransform>();
        skullPanel = transform.Find("Skull").gameObject;
        skullImage = skullPanel.GetComponent<Image>();
        ship = GameObject.Find("ShipManager").GetComponent<ShipManager>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            transform.parent.Find("OptionsPanel").gameObject.SetActive(true);
        }

        threadDistance = zeppelinProgress - skullProgress;
        zeppelinProgress = zeppelinProgress + speedScaling * ship.speed * Time.deltaTime;
        if(zeppelinProgress > 1)
        {
            zeppelinProgress = 1;
        }
        if (scull)
        {
            float skspeed = ScullSpeed;
            if (!ship.engineRunning)
            {
                skspeed = skspeed * 2;
            }
            skullProgress = skullProgress + skspeed * Time.deltaTime;
            if(skullProgress > 1)
            {
                skullProgress = 1;
            }
        }
        if(scull && skullProgress > zeppelinProgress)
        {
            GameOver();
            return;
        }
        if(zeppelinProgress > 0.95)
        {
            Win();
            return;
        }
        if(zeppelinProgress > ScullThreshold)
        {
            scull = true;
        }
    }

    private void FixedUpdate()
    {
        zeppelinPanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(calcPosition(zeppelinProgress), 0, 0);
        skullPanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(calcPosition(skullProgress), 0, 0);
        if (skullImage.enabled == false && zeppelinProgress > 0.1)
        {
            skullImage.enabled = true;
        }
    }

    private float calcPosition(float progress)
    {
        return Mathf.Max(0, myTransform.rect.width - zeppelinTRansform.rect.width) * progress;
    }

    private void GameOver()
    {
        FindObjectOfType<PanicManager>().paused = true;
        FindObjectOfType<AudioManager>().SwitchToMusicStage(-1);

        transform.parent.Find("GameOverPanel").gameObject.SetActive(true);
        Debug.Log("LooOOOOoooOOOOoooOOOOOst");
    }

    private void Win()
    {
        FindObjectOfType<PanicManager>().paused = true;
        FindObjectOfType<AudioManager>().SwitchToMusicStage(0);
        Debug.Log("SurviiiiiiiiIIIIIiiiIIIIIIiiiived");
        transform.parent.Find("WinningPanel").gameObject.SetActive(true);
    }
}
