﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class TrailManager : MonoBehaviour
{
    [Range(0f, 40f)]
    public float targetSpeed;

    private float shipSpeed;

    public ParticleSystem[] trails;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        AdjustShipSpeed();
        foreach(ParticleSystem trail in trails)
        {
            if (trail == null)
            {
                continue;
            }
            var ps = trail.velocityOverLifetime;
            ps.speedModifier = shipSpeed;
        }

    }

    private void AdjustShipSpeed()
    {
        if (targetSpeed < shipSpeed)
        {
            shipSpeed -= 0.1f;
        }
        else if (targetSpeed > shipSpeed)
        {
            shipSpeed += 0.1f;
        }
    }
}
