﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.Experimental.VFX;

public class WheelController : MonoBehaviour
{
    // Start is called before the first frame update
    private float measureCylinderDefault = -1.1f;
    private float maxMeasure = 2.0f;
    public GameObject measureCylinder;

    private int close_player_count = 0;

    public float rotation_speed = 0.1f;

    public float rotation_value = 0;
    public float rotation_scaling = 10f;
    public float rotation_scaling_negative = 10f;
    private Quaternion last_rotation;

    private ShipManager shipmanager;

    private VisualEffect visualEffects;
    private float last_rotation_value = 0;
    public float exclamation_threshold = 0.3f;

    // Start is called before the first frame update
    void Start()
    {
        last_rotation = transform.rotation;
        measureCylinderDefault = measureCylinder.transform.position.y;
        shipmanager = GameObject.Find("ShipManager").GetComponent<ShipManager>();
        visualEffects = GetComponentInChildren<VisualEffect>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        //float delta_rot = last_rotation - transform.rotation.y;
        
        if(transform.rotation == last_rotation)
        {
            //Dont update last rotation if increment was too small
        }
        else
        {
            // get a "forward vector" for each rotation
            var forwardA = transform.rotation * Vector3.forward;
            var forwardB = last_rotation * Vector3.forward;
            // get a numeric angle for each vector, on the X-Z plane (relative to world forward)
            var angleA = Mathf.Atan2(forwardA.x, forwardA.z) * Mathf.Rad2Deg;
            var angleB = Mathf.Atan2(forwardB.x, forwardB.z) * Mathf.Rad2Deg;
            // get the signed difference in these angles
            var angleDiff = Mathf.DeltaAngle(angleA, angleB);
            if(angleDiff < 0)
            {
                rotation_value = rotation_value + rotation_scaling_negative * angleDiff * Time.deltaTime;
            }
            else
            {
                rotation_value = rotation_value + rotation_scaling * angleDiff * Time.deltaTime;
            }

            if(rotation_value > exclamation_threshold && last_rotation_value <= exclamation_threshold)
            {
                Debug.Log("StopAlarm");
                FindObjectOfType<AudioManager>().MuteSound("Alarm", true);
                //stop playing
                visualEffects.Stop();
            }
            if (rotation_value < exclamation_threshold && last_rotation_value >= exclamation_threshold)
            {
                Debug.Log("StartAlarm");
                FindObjectOfType<AudioManager>().Play("Alarm");
                visualEffects.Play();
            }
            //update rotation value and last rotation
            last_rotation = transform.rotation;
            last_rotation_value = rotation_value;

        }


        if (close_player_count < 1 && shipmanager.speed > 0 && rotation_value > 0)
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * rotation_speed);
        }
        if(rotation_value < 0)
        {
            shipmanager.EngineBreackdown();
            rotation_value = 0;
        }
        if(rotation_value > maxMeasure)
        {
            shipmanager.startEngine();
            rotation_value = maxMeasure;
        }
        measureCylinder.transform.position = new Vector3(measureCylinder.transform.position.x, measureCylinderDefault + rotation_value,measureCylinder.transform.position.z);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "Player")
    //    {
    //        close_player_count = close_player_count + 1;
    //    }
    //}
    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.tag == "Player")
    //    {
    //        close_player_count = close_player_count - 1;
    //    }
    //}
}
