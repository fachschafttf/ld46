﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanicManager : MonoBehaviour
{
    int currentFrameCount = 0;
    int maxPanicStage = 5;

    public bool paused;

    [Range(2,6)]
    public int flameFactor;
    
    // Start is called before the first frame update
    void Start()
    {
        paused = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void FixedUpdate()
    {
        currentFrameCount = (currentFrameCount + 1) % 50;
        if (!paused && currentFrameCount == 0)
        {
            UpdatePanicStage();
        }
    }

    private void UpdatePanicStage()
    {
        int flameEscalation = EvaluateFlames();
        int boilerEscalation = EvaluateBoiler();
        int pressureEscalation = EvaluatePressure();
        int fueltankEscalation = EvaluateFuelTank();
        int wheelEscalation = EvaluateWheel();

        int panicStage = flameEscalation + boilerEscalation + fueltankEscalation + pressureEscalation + wheelEscalation + 1;

        FindObjectOfType<AudioManager>().SwitchToMusicStage(Math.Min(panicStage, maxPanicStage));
    }

    private int EvaluatePressure()
    {
        PressureController pressure = FindObjectOfType<PressureController>();

        return pressure.pressure / pressure.MaximalPressure > pressure.ExclamationThreshold ? 1 : 0;
    }

    private int EvaluateWheel()
    {
        WheelController wheel = FindObjectOfType<WheelController>();
        int panicStage = 0;

        float currentValue = wheel.rotation_value;
        float warningValue = wheel.exclamation_threshold;
        float minValue = 0;

        if (currentValue < minValue)
        {
            panicStage = 2;
        }
        else if (currentValue < warningValue)
        {
            panicStage = 1;
        }
        //Debug.Log("Wheel: " + panicStage);
        return panicStage;
    }

    private int EvaluateFuelTank()
    {
        FuelControll tank = FindObjectOfType<FuelControll>();

        float panicTreshhold = 0.2f;    
        //Debug.Log("Fuel: " + tank.fillLevel);
        return tank.fillLevel < panicTreshhold ? 1 : 0;
    }

    private int EvaluateBoiler()
    {
        BoilerController boiler = FindObjectOfType<BoilerController>();

        //Debug.Log("Heat: " + boiler.heat);
        return boiler.heat > 0.8f * boiler.overheat_threshold ? 1 : 0;
    }

    private int EvaluateFlames()
    {
        int flameCount = GameObject.FindGameObjectsWithTag("Flame").Length;
        //Debug.Log(flameCount + " Flames");
        return (int) Math.Max(0, Math.Log(flameCount, flameFactor));
    }
}
