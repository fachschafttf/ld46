﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireStarter : MonoBehaviour
{

    public GameObject flame;
    private GameObject audiomanager;
    // Start is called before the first frame update
    void Start()
    {
        audiomanager = GameObject.Find("AudioManager");
        //StartFire(new Vector3(0, 0, 0));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartFire(Vector3 position)
    {
        position = position + new Vector3(0, 0.5f, 0);
        Instantiate(flame, position, Quaternion.identity);
    }
}
