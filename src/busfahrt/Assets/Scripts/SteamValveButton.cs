﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamValveButton : MonoBehaviour
{
    public GameObject Boiler;
    public bool buttonPressed;
    private PressureController pressureController;

    void Start()
    {
        pressureController = Boiler.GetComponent<PressureController>();
        buttonPressed = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            ButtonPressing();
            buttonPressed = true;
        }
    }

    private void ButtonPressing()
    {
        pressureController.SteamButtonPressing();
    }
}
