﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour
{

    #region FIELDS
    public GameObject BlackImagePrefab;
    public FadeDirection Direction;
    private Image fadeOutUIImage;
    public float fadeSpeed = 0.8f;
    public int WaitLoopsBeforeFade = 0;
    public string SceneToLoad = "";
    public int SceneToLoadBuildIndex = -1;
    private int _waitedLoops;
    private bool _started = false;

    public enum FadeDirection
    {
        In, //Alpha = 1
        Out // Alpha = 0
    }
    // Start is called before the first frame update
    void Start()
    {
        fadeOutUIImage = Instantiate(BlackImagePrefab, new Vector3(0, 0, 0), Quaternion.identity).transform.Find("BlackImage").GetComponent<Image>();        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_waitedLoops > WaitLoopsBeforeFade && !_started)
        {
            if (SceneToLoad == "" && SceneToLoadBuildIndex == -1)
            {
                StartCoroutine(Fade(Direction));
            }
            else
            {
                if (SceneToLoadBuildIndex != -1)
                {
                    StartCoroutine(FadeAndLoadScene(Direction, SceneToLoadBuildIndex));
                }
                else
                {
                    StartCoroutine(FadeAndLoadScene(Direction, SceneToLoad));
                }
            }
        }
        _waitedLoops++;
    }

    #region FADE
    private IEnumerator Fade(FadeDirection fadeDirection)
    {
        float alpha = (fadeDirection == FadeDirection.Out) ? 1 : 0;
        float fadeEndValue = (fadeDirection == FadeDirection.Out) ? 0 : 1;
        if (fadeDirection == FadeDirection.Out)
        {
            while (alpha >= fadeEndValue)
            {
                SetColorImage(ref alpha, fadeDirection);
                yield return null;
            }
            fadeOutUIImage.enabled = false;
        }
        else
        {
            fadeOutUIImage.enabled = true;
            while (alpha <= fadeEndValue)
            {
                SetColorImage(ref alpha, fadeDirection);
                yield return null;
            }
        }
    }
    #endregion

    #region HELPERS
    public IEnumerator FadeAndLoadScene(FadeDirection fadeDirection, string sceneToLoad)
    {
        yield return Fade(fadeDirection);
        SceneManager.LoadScene(sceneToLoad);
    }

    public IEnumerator FadeAndLoadScene(FadeDirection fadeDirection, int sceneToLoad)
    {
        yield return Fade(fadeDirection);
        SceneManager.LoadScene(sceneToLoad);
    }

    private void SetColorImage(ref float alpha, FadeDirection fadeDirection)
    {
        fadeOutUIImage.color = new Color(fadeOutUIImage.color.r, fadeOutUIImage.color.g, fadeOutUIImage.color.b, alpha);
        alpha += Time.deltaTime * (1.0f / fadeSpeed) * ((fadeDirection == FadeDirection.Out) ? -1 : 1);
    }
    #endregion

    #endregion

}
