﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.Experimental.VFX;
using System;

public class FlameExpansion : MonoBehaviour
{
    public Transform x_pos;
    public Transform x_neg;
    public Transform z_pos;
    public Transform z_neg;

    private GameObject firestarter;

    private Transform[] spread_check;

    public float growth_rate;
    public float initial_threshold;

    private float threshold;
    // Start is called before the first frame update
    void Start()
    {
        threshold = initial_threshold;
        spread_check = new Transform[4] { x_pos, x_neg, z_pos, z_neg };
        firestarter = GameObject.Find("Firestarter");
    }

    // Update is called once per frame
    void Update()
    {
        threshold = threshold - growth_rate * Time.deltaTime;
        if(threshold < 0)
        {
            SpreadFire();
            threshold = initial_threshold;
        }
    }

    void SpreadFire()
    {
        foreach (Transform fireCheck in spread_check)
        {
            //Debug.Log(fireCheck);
            Collider[] hit_fires = Physics.OverlapSphere(fireCheck.position, 0.4f);
            bool isfire = false;
            foreach(Collider fire in hit_fires)
            {
                if(fire.gameObject.tag == "Flame")
                {
                    isfire = true;//do nothing
                }             
            }
            if (!isfire && InFlammableArea(fireCheck.position))
            {
                //Debug.Log("spawn fire!");
                firestarter.GetComponent<FireStarter>().StartFire(fireCheck.position- new Vector3(0,0.5f, 0));
            }
        }
    }

    private bool InFlammableArea(Vector3 spawnPosition)
    {
        bool inArea = true;

        Vector3 corner1 = new Vector3(-12.6f, 0, 4.24f);
        Vector3 corner2 = new Vector3(9.2f, 0, -3.61f);

        inArea &= corner1.x < spawnPosition.x;
        inArea &= corner1.z > spawnPosition.z;
        inArea &= corner2.x > spawnPosition.x;
        inArea &= corner2.z < spawnPosition.z;




        return inArea;
    }

    public void extinguish()
    {
        UnityEngine.Object.Destroy(this.gameObject);
    }
}
