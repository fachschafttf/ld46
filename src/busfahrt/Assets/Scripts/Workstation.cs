﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Workstation : MonoBehaviour
{
    public bool IsItemSource;
    [Tooltip("The Rotation of the item that is generated here.")]
    public Quaternion ItemRotation;

    [Tooltip("The Item that gets spawned from this station if player tries to")]
    public Item Item;

    public Item GiveNewItem()
    {
        if (IsItemSource)
        {
            //Debug.Log("here you go");
            return Instantiate(Item, new Vector3(0, 0, 0), ItemRotation);
            
        }

        return null;
    }
}
