﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour
{
    public float speed = 0f;
    public float initial_speed = 10f;

    public bool engineRunning = false;

    public GameObject rotor1;
    private RotorRotation rotor;
    private TrailManager trails;

    public float speedGained =0f;
    public float acceleration = 0.5f;
    public float maxSpeed = 25;
    public int guiSpeed;
    private int lastGuiSpeed;

    public UnityEngine.UI.Text textField;
    public UnityEngine.UI.Text secondLine;

    // Start is called before the first frame update
    void Start()
    {
        //initalize values
        rotor = rotor1.GetComponent<RotorRotation>();
        trails = GameObject.Find("TrailManager").GetComponent<TrailManager>();
        guiSpeed = (int) speed;
        lastGuiSpeed = guiSpeed;
        textField.text = "SPEED: " + guiSpeed.ToString();
        secondLine.text = "START ENGINE";
    }

    // Update is called once per frame
    void Update()
    {
        if (engineRunning)
        {
            speedGained = speedGained + acceleration * Time.deltaTime;
            setSpeed(Mathf.Min(maxSpeed, initial_speed + speedGained));
        }
        //update rotor when needed
        //rotor.SetRotationSpeed(speed);
    }

    public void startEngine()
    {
        if (!engineRunning)
        {
            //TODO: stuff 
            setSpeed(initial_speed);
            engineRunning = true;
            Debug.Log("Engine started Running");
        }
    }

    public void EngineBreackdown()
    {
        if (engineRunning)
        {
            //TODO: stuff
            setSpeed(0f);
            textField.text = "ENGINE FAILURE";
            secondLine.text = "RESTART ENGINE";
            engineRunning = false;
            speedGained = 0f;
            Debug.Log("Engine Breacks Down!!!");
        }
    }

    private void setSpeed(float _speed)
    {
        speed = _speed;
        rotor.SetRotationSpeed(speed);
        trails.targetSpeed = speed;
        guiSpeed = (int)speed;
        if(guiSpeed != lastGuiSpeed)
        {
            textField.text = "SPEED: " + guiSpeed.ToString();
            secondLine.text = "";
            lastGuiSpeed = guiSpeed;
        }
    }
}
