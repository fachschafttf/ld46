﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketController : MonoBehaviour
{
    public float extinguish_radius;
    public int maxCharges;
    public HUDBar hudbar;

    public GameObject BarelOpen;
    private Renderer barelRend;

    private ParticleSystem splashEffect;

    private int waterCharges;

    public void Start() {
        splashEffect = GameObject.Find("Splash").GetComponent<ParticleSystem>();
        waterCharges = maxCharges;
        if (hudbar) {
            hudbar.SetMaxValue(maxCharges);
            hudbar.SetValue(waterCharges);
        }
        barelRend = BarelOpen.GetComponent<Renderer>();

    }

    private void ChangeColorToEmpty()
    {
        barelRend.material.color = Color.red;
    }

    private void ChangeColorToFull()
    {
        barelRend.material.color = Color.white;
    }

    public void UseItem()
    {
        ChangeColorToEmpty();
        bool nearWaterRefillStation = IsNearWaterRefillStation();

        if (nearWaterRefillStation)
        {
            FindObjectOfType<AudioManager>().Play("FillUp");
            RefillWater();
        }
        else
        {
            EmptyBucketInEnvironment();
        }
    }

    private bool IsNearWaterRefillStation()
    {
        Collider[] affectedObjectsInRange = Physics.OverlapSphere(transform.position, 2f);

        foreach (Collider affectedObject in affectedObjectsInRange)
        {
            if (affectedObject.gameObject.tag == "WaterBarrel")
            {
                return true;
            }
        }

        return false;
    }

    private void RefillWater()
    {
        waterCharges = maxCharges;
        ChangeColorToFull();
    }

    private void EmptyBucketInEnvironment()
    {
        if (waterCharges > 0)
        {
            if (waterCharges == 1)
            {
                ChangeColorToEmpty();
            }
            splashEffect.Play();

            waterCharges -= 1;

            Collider[] affectedObjectsInRange = Physics.OverlapSphere(transform.position, extinguish_radius);

            foreach (Collider affectedObject in affectedObjectsInRange)
            {
                string objectTag = affectedObject.gameObject.tag;

                switch (objectTag)
                {
                    case "Flame":
                        affectedObject.GetComponent<FlameExpansion>().extinguish();
                        break;
                    case "Boiler":
                        affectedObject.GetComponent<BoilerController>().extinguish();
                        break;
                }
            }
            if (hudbar)
            {
                hudbar.SetValue(waterCharges);
            }
            FindObjectOfType<AudioManager>().Play("Splash");
        }
        else
        {
            FindObjectOfType<AudioManager>().Play("ErrorSound");
        }
    }
}
