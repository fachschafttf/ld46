﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Runtime.InteropServices;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    private Item _item;

    public Item item
    {
        get { return _item; }
        set
        {
            // Play Sound on Item Selection
            if (item != null || value != null)
            {
                FindObjectOfType<AudioManager>().Play("SelectItem");
            }
            _item = value;
        }
    }
    private Vector3 itemDropPos;
    private Vector3 itemCarryPos;

    public float FloorHeight;
    public float GrabRange;
    public LayerMask itemLayer;
    public LayerMask workstationLayer;

    private GameObject fuelTank;

    private void Start()
    {
        itemDropPos = new Vector3(0, 0.5f, 0);
        itemCarryPos = transform.position + transform.forward;
        fuelTank = GameObject.Find("FuelTank");
    }

    void Update()
    {
        //Debug.Log(item);
        UpdatePositions();
        
        // UPDATE POSITION OF ITEM IN HAND -> last thing we do
        if (item != null)
        {
            item.SetPosition(itemCarryPos);
        }
    }
    
    public void ChangeItem()
    {
        // we have an item in hand: 2 posibilities
        if (item != null)
        {
            bool item_placed_at_workstation = false;
            var workstation = GetClosestWorkstation();
            // 1: we want to place it at the workstation
            if (workstation != null)
            {
                var station = workstation.tag;
                // check which station we are closest to
                switch (station)
                {
                    //case "Furnace":
                    //    // place the log in the furnace if possible
                    //    var furnace = workstation.GetComponent<FurnaceController>();
                    //    if (furnace.FreeSpace()) {
                    //        furnace.PlaceLog(item);#
                    //        item_placed_at_workstation = true;
                    //        item = null;
                    //    }
                    //    else
                    //    {
                    //        //goto default;
                    //    }
                    //    break;
                    case "FuelEnricher":

                        // check if its a fitting type
                        if (item.ItemType == Item.ItemTypes.fuelUnprocessed)
                        {
                            // place the fuel inside the pot if its empty
                            var enricher = workstation.GetComponent<FuelEnricherController>();
                            if (enricher.Status == FuelEnricherController.EnricherStatus.empty)
                            {
                                enricher.PlaceNewItem(item);
                                item_placed_at_workstation = true;
                                item = null;
                            }
                            else
                            {
                                goto default;
                            }
                        }
                        else
                        {
                            goto default;
                        }

                        break;
                    case "FuelTank":
                        if (item.ItemType == Item.ItemTypes.fuelFin)
                        {
                            fuelTank.GetComponent<FuelControll>().insertFuel();
                            item_placed_at_workstation = true;
                            GameObject go = item.gameObject;
                            item = null;
                            GameObject.Destroy(go);
                        }
                        break;
                    default:
                        item.transform.position = workstation.transform.position;
                        break;
                }
            }
            if (!item_placed_at_workstation)
            {
                /* SWAPPING ITEMS IS REMOVED WITH VERSION DAY2.smth
                var swap = GetClosestItem();
                // 2: we want to exchange an item with an item on the floor
                if (swap != null)
                {
                    var newItem = GetClosestItem();
                    // place the old item at new items position
                    item.transform.position = newItem.transform.position;
                    item = newItem;
                }
                else
                */

                bool drop = true;
                // 3: we want to drop the item
                if (drop)
                {
                    item.transform.position = itemDropPos;
                    item = null;
                }
            }
        }
        // no item in inventory
        else
        {
            // try to pick up a item
            item = GetClosestItem();
            
            // try to spawn a new item from a source
            if (item == null)
            {
                var source = GetClosestWorkstation();
                
                if (source != null) {
                    var station = source.tag;
                    // check which station we are closest to
                    switch (station) {
                        case "FuelEnricher":
                            var enricher = source.GetComponent<FuelEnricherController>();
                            item = enricher.giveNewItem();
                            break;
                        default:
                            item = source.GiveNewItem();
                            break;
                    }
                }
            }
        }
    }

    Item GetClosestItem()
    {
        Collider[] itemsInRange = Physics.OverlapSphere(itemCarryPos, GrabRange, itemLayer);
        Item closestItem = null;
        float closestDist = Mathf.Infinity;
        
        foreach (var currItem in itemsInRange)
        {
            // dont count the item currently in inventory
            if (item == currItem.GetComponent<Item>())
            {
                continue;
            }
            
            var dist = Vector3.Distance(currItem.transform.position, itemCarryPos);
            if (dist < closestDist)
            {
                closestDist = dist;
                closestItem = currItem.GetComponent<Item>();
            }
        }
        return closestItem;
    }
    
    Workstation GetClosestWorkstation()
    {
        Collider[] workstationsInRange = Physics.OverlapSphere(itemCarryPos, GrabRange, workstationLayer);
        Workstation closestWorkstation = null;
        float closestDist = Mathf.Infinity;
        
        foreach (var workstation in workstationsInRange)
        {
            var dist = Vector3.Distance(workstation.transform.position, itemCarryPos);
            if (dist < closestDist)
            {
                closestDist = dist;
                closestWorkstation = workstation.GetComponent<Workstation>();
            }
        }
        return closestWorkstation;
    }

    void UpdatePositions()
    {
        itemCarryPos = transform.position + 0.7f * transform.forward + new Vector3(0, 0.7f, 0);
        itemDropPos = new Vector3(itemCarryPos.x, FloorHeight, itemCarryPos.z);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(itemCarryPos, GrabRange);
    }
}
