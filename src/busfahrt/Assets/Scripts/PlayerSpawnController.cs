﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]

public class PlayerSpawnController : MonoBehaviour
{
    public GameObject playerPrefab;
    public PlayerManager playerManagerPrefab;
    private PlayerManager _playerManager;


    // Start is called before the first frame update
    void Start()
    {
        foreach (string name in Input.GetJoystickNames())
        {
            Debug.Log("GamePad initialized: " + name);
        }
        _playerManager = (PlayerManager)GameObject.FindObjectOfType(typeof(PlayerManager));
        if (_playerManager == null)
        {
            Debug.LogWarning("You seem not to have a PlayerManager in your scene. You can not add Players");
            _playerManager = Instantiate(playerManagerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_playerManager != null) { 
            int newPlayer = PlayerAssign();
            if (newPlayer >= 0)
            {
                GameObject spawnpoint = GameObject.Find("SpawnPoint" + (newPlayer + 1));
                GameObject newPlayerObject = Instantiate(playerPrefab, spawnpoint.transform.position, Quaternion.identity);
                newPlayerObject.GetComponent<PlayerController>().PlayerNumber = newPlayer;
            }
        }
    }

    int PlayerAssign()
    {
        if (!_playerManager.inputDeactivated)
        {
            for (int i = 0; i < _playerManager.Players.Length; i++)
            {
                Player player = _playerManager.Players[i];
                if (player.GamepadNumber == -1)
                {
                    int newGamepad = -1;
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        newGamepad = 0;
                    }
                    for (int j = 1; j < 17; j++)
                    {
                        if (Input.GetKeyDown("joystick " + j + " button " + 0))
                        {
                            newGamepad = j;
                        }
                    }
                    if (newGamepad > -1)
                    {
                        foreach (Player p in _playerManager.Players)
                        {
                            if (p.GamepadNumber == newGamepad)
                            {
                                return -1; // someone else already uses this gamepad
                            }
                        }
                        _playerManager.Players[i].GamepadNumber = newGamepad;
                        Debug.Log("Controller " + newGamepad + " is assigned to player " + _playerManager.Players[i].PlayerNumber + _playerManager.inputDeactivated);
                        return i;

                    }
                }
            }
        }
        return -1;
    }
}
