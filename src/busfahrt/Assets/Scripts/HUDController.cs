﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour {
    public int GamepadNumber = -1;   // 0 for keyboard
    private Vector3 movement;
    private bool dash;
    public HUDBar hb1, hb2;
    private float v1, v2;


    // Start is called before the first frame update
    void Start() {
        v1 = 100;
        v2 = 0;
        hb1.SetMaxValue(100);
        hb2.SetMaxValue(100);
    }

    void FixedUpdate() {
        ButtonHandler();
        AxisHandler();
        move();
    }

    void AxisHandler() {
        string horizontalAxis = "Horizontal";
        string verticalAxis = "Vertical";

        if (GamepadNumber > 0) // no keyboard
        {
            horizontalAxis = "HorizontalJoy" + GamepadNumber;
            verticalAxis = "VerticalJoy" + GamepadNumber;
        }
        movement = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));

    }

    void ButtonHandler() {
        string dashKey = GamepadNumber == 0 ? "space" : "joystick " + GamepadNumber + " button " + 1;
        dash = Input.GetKeyDown(dashKey);
    }


    private void move() {
        if(movement.x > 0) {
            v1 += 1;
        } else if(movement.x < 0) {
            v1 -= 1;
        } if(movement.z > 0) {
            v2 += 1;
        } else if (movement.z < 0) {
            v2 -= 1;
        }
        hb1.SetValue(v1);
        hb2.SetValue(v2);
    }
}
