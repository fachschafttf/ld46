﻿using System.Collections;
using System.Collections.Generic;
using TMPro.Examples;
using UnityEngine;
using UnityEngine.UI;

public class Description : MonoBehaviour
{
    public string Text = "Press Space\nor";
    public GameObject description;
    public int PlayerNumber = 0;
    private bool noKeyboard = false;
    // Start is called before the first frame update
    void Start()
    {
        description = gameObject;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {

        PlayerManager playerManager = (PlayerManager)GameObject.FindObjectOfType(typeof(PlayerManager));
        if (!noKeyboard)
        {
            foreach (Player player in playerManager.Players)
            {
                if (player.GamepadNumber == 0)
                {
                    ((Text)description.transform.GetComponent("Text")).text = "Press";
                    noKeyboard = true;
                }
            }
        }
        if (playerManager.Players[PlayerNumber].GamepadNumber > -1)
        {
            description.SetActive(false);
        }
    }
}
