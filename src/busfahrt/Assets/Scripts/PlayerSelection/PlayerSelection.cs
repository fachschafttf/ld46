﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerSelection : MonoBehaviour
{
    PlayerManager _playerManager;
    public GameObject StartGame;
    public GameObject Fader;

    // Start is called before the first frame update
    void Start()
    {
        _playerManager = (PlayerManager)GameObject.FindObjectOfType(typeof(PlayerManager));
        if (Fader != null)
        {
            Fader.GetComponent<FadeInOut>().Direction = FadeInOut.FadeDirection.In;
            Fader.GetComponent<FadeInOut>().SceneToLoad = "CombinedScene2";
            //Fader.GetComponent<FadeInOut>().SceneToLoadBuildIndex = SceneManager.GetActiveScene().buildIndex + 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_playerManager.PlayerCount > 0)
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown("joystick button 7"))
            {

                if (Fader == null)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
                }
                else
                {
                    Fader.SetActive(true);
                }


            }
            StartGame.SetActive(true);

        }

        if (_playerManager != null)
        {
            int newPlayer = PlayerAssign();
        }
    }


    int PlayerAssign()
    {

        for (int i = 0; i < _playerManager.Players.Length; i++)
        {
            Player player = _playerManager.Players[i];
            if (player.GamepadNumber == -1)
            {
                int newGamepad = -1;
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    newGamepad = 0;
                }
                for (int j = 1; j < 17; j++)
                {
                    if (Input.GetKeyDown("joystick " + j + " button " + 0))
                    {
                        newGamepad = j;
                    }
                }
                if (newGamepad > -1)
                {
                    foreach (Player p in _playerManager.Players)
                    {
                        if (p.GamepadNumber == newGamepad)
                        {
                            return -1; // someone else already uses this gamepad
                        }
                    }
                    _playerManager.Players[i].GamepadNumber = newGamepad;
                    Debug.Log("Controller " + newGamepad + " is assigned to player " + _playerManager.Players[i].PlayerNumber);
                    return i;
                }
            }
        }
        return -1;
    }
}
