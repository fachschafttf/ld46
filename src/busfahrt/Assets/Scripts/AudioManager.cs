﻿using UnityEngine;
using System;
using System.Security.Cryptography;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{

    [Range(0f, 1f)]
    public float masterVolume;

    [Range(0f, 1f)]
    public float musicVolume;

    [Range(0f, 1f)]
    public float effectVolume;

    [Range(0f, 1f)]
    public float ambientVolume;

    public Sound[] musicSounds;
    public Sound[] ambientSounds;
    public Sound[] effectSounds;

    [Range(-1, 5)]
    public int initStage;

    List<Sound> allSounds;

    Dictionary<int, string> stageMusic = new Dictionary<int, string>()
    {
        { -1, "GameOver" },
        { 0, "MainMenu" },
        { 1, "Stage1" },
        { 2, "Stage2" },
        { 3, "Stage3" },
        { 4, "Stage4" },
        { 5, "Stage5" }
    };

    string[] effectMusic = new string[] { "FireMusic" };

    public static AudioManager instance;

    // Start is called before the first frame update
    void Awake()
    {
        // Keep same Audiomanager between sceneswitches
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        allSounds = GetAllSounds();
        RegisterSounds();
    }

    public List<Sound> GetAllSounds()
    {
        List<Sound> sounds = new List<Sound>();
        sounds.AddRange(musicSounds);
        sounds.AddRange(ambientSounds);
        sounds.AddRange(effectSounds);

        return sounds;
    }

    private void RegisterSounds()
    {
        foreach (Sound sound in allSounds)
        {
            RegisterSound(sound);
        }
    }

    private void RegisterSound(Sound sound)
    {
        sound.source = gameObject.AddComponent<AudioSource>();
        sound.source.clip = sound.clip;
        sound.source.volume = sound.volume * masterVolume;
        sound.source.loop = sound.loop;
    }

    private void Start()
    {
        // Register Sounds that should be played on gamestart
        StartStageMusicLoops();
        StartWindLoop();
        StartEffectMusicLoops();
    }

    private void FixedUpdate()
    {
        AdjustSoundVolumes();
    }

    private void AdjustSoundVolumes()
    {
        Dictionary<Sound[], float> soundsWithFactors = new Dictionary<Sound[], float> ()
        {
                { musicSounds, musicVolume },
                { ambientSounds, ambientVolume},
                { effectSounds, effectVolume }
        };
        foreach (KeyValuePair<Sound[], float> pair in soundsWithFactors)
        {
            foreach (Sound sound in pair.Key)
            {
                sound.source.volume = sound.mute ? 0 : sound.volume * masterVolume * pair.Value;
            }
        }
    }

    // -1 Gameover, 0 Mainmenu, 1-... Stages
    public void SwitchToMusicStage(int stageNum)
    {
        // Mute all playing music tracks
        //Debug.Log("Switching to stage: " + stageNum);
        foreach (KeyValuePair<int, string> stage in stageMusic)
        {
            MuteSound(stage.Value, stageNum != stage.Key);
        }
    }

    public void MuteSound(string name, bool mute)
    {
        Sound sound = Array.Find(allSounds.ToArray(), s => s.name == name);
        if (sound == null)
        {
            Debug.LogWarning("Sound: " + name + " not found");
            return;
        }
        sound.mute = mute;
    }

    public void Play (string name, bool startMuted=false)
    {
        Sound sound = Array.Find(allSounds.ToArray(), s => s.name == name);
        if (sound == null)
        {
            Debug.LogWarning("Sound: " + name + " not found");
            return;
        }
        sound.mute = startMuted;
        sound.source.Play();
    }


    public void StartStageMusicLoops()
    {
        foreach (KeyValuePair<int, string> stage in stageMusic)
        {
            // Start all muted except MainMenu
            Play(stage.Value, stage.Key != initStage);
        }
    }

    public void StartEffectMusicLoops()
    {
        foreach (string effect in effectMusic)
        {
            Play(effect, true);
        }
    }


    public void StartWindLoop ()
    {
        Play("Wind1");
        Play("Wind2");
        Play("Wind2");
        Play("Wind2");
    }
}
