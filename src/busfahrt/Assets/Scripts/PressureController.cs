﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.Experimental.VFX;

public class PressureController : MonoBehaviour
{
    private bool buttonIsPressed;
    private BoilerController boiler;
    public BarometerInteravtive barometer;
    public float PressureIncreaseValue = 0.08f;
    [Tooltip("The pressure decreases exponentially while the button is pressed; this is the percent how much pressure stays inside: 0 -> instantly reset; 1-> unchanged")]
    public float PressureDecreasePercent = 0.95f;
    public float MaximalPressure = 100f;
    public float pressure;
    public ParticleSystem Steam;
    public float PressureNeededForParticle;
    private VisualEffect visualEffects;
    public float ExclamationThreshold = 0.9f;
    public CameraShaker camerashaker;
    private ShipManager ship;


    void Start()
    {
        boiler = GetComponent<BoilerController>();
        Steam.Stop();
        visualEffects = GetComponentInChildren<VisualEffect>();
        visualEffects.Stop();
        ship = GameObject.Find("ShipManager").GetComponent<ShipManager>();
    }

    void FixedUpdate()
    {
        if (buttonIsPressed)
        {
            var oldValue = pressure;
            pressure *= PressureDecreasePercent;
            var newValue = pressure;

            // es spritzt nur wenn auch ordentlich druck auf der leitung ist
            if (oldValue - newValue > PressureNeededForParticle)
            {
                if (Steam.isPlaying == false)
                {
                    Steam.Play();
                }
            }

            buttonIsPressed = false;
        }
        else
        {
            var currentPressureIncrease = boiler.heatPercentage * PressureIncreaseValue;
            pressure += currentPressureIncrease;
            // Debug.Log(pressure);
            if (pressure > MaximalPressure)
            {
                pressure = MaximalPressure;
                Overheat();
            }
        }

        // Warning:
        if (pressure / MaximalPressure > ExclamationThreshold)
        {
            // Debug.Log("Where are those damn!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            // Debug.Log("visualEffect: " + visualEffects);
            visualEffects.Play();
        }
        else
        {
            visualEffects.Stop();
        }

        // Debug.Log("Pressure is " + pressure);
        SetBarometer();
    }

    public void SteamButtonPressing()
    {
        buttonIsPressed = true;
    }

    void SetBarometer()
    {
        barometer.pressure = (pressure / MaximalPressure) * 100f;
    }

    void Overheat()
    {
        camerashaker.SmallShake();
        ship.EngineBreackdown();
        pressure = 0f;
    }
}
