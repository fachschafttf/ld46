﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    private GameObject _playButton;
    private AudioManager audioManager;
    public Slider MasterSlider;
    public Slider MusicSlider;
    public Slider EffectsSlider;
    // Start is called before the first frame update
    

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadOtherScene()
    {
        SceneManager.LoadScene("CombinedScene1", LoadSceneMode.Single);
    }

    public void LoadTutorialScene()
    {
        SceneManager.LoadScene("Tutorial", LoadSceneMode.Single);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private void Start()
    {
        audioManager = FindObjectOfType<AudioManager>();
        MasterSlider.value = audioManager.masterVolume;
        MusicSlider.value = audioManager.musicVolume;
        EffectsSlider.value = audioManager.effectVolume;
        audioManager.ambientVolume = EffectsSlider.value;
    }
    public void MasterVolume()
    {
        audioManager.masterVolume = MasterSlider.value;
    }
    public void MusicVolume()
    {
        audioManager.musicVolume = MusicSlider.value;
    }
    public void EffectsVolume()
    {
        audioManager.effectVolume = EffectsSlider.value;
        audioManager.ambientVolume = EffectsSlider.value;
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);

    }
}
