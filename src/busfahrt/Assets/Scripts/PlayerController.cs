﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;


public class PlayerController : MonoBehaviour
{
    private int _PlayerNumber = -1;
    private Player _player;
    public float Speed;
    public float DashVelocity;
    public float dashCooldown;
    public float turnRate;
    private Rigidbody body;
    private CharacterController controller;
    private PlayerInventory playerInventory;
    private Vector3 movement;
    private bool isDash;
    private float sinceDash;
    private RespawnController respawner;
    private VisualEffect visualEffect;

    public int PlayerNumber
    {
        get { return this._PlayerNumber; }
        set {
            this._PlayerNumber = value;
            SkinnedMeshRenderer mesh = transform.Find("ConeHuman/Cone").GetComponent<SkinnedMeshRenderer>();
            Material baseMaterial = (Material)Resources.Load("ColorPalette", typeof(Material));
            Material playerMaterial = (Material)Resources.Load("PlayerColor" + (this._PlayerNumber + 1), typeof(Material));
            Material[] materials = new Material[] { playerMaterial, baseMaterial};
            mesh.materials = materials;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody> ();
        sinceDash = dashCooldown;
        playerInventory = GetComponent<PlayerInventory>();
        if(!TryGetComponent<RespawnController>(out respawner)){
            respawner = new RespawnController();
        }
        visualEffect = GetComponentInChildren<VisualEffect>();
        visualEffect.playRate = 5;
    }

    void FixedUpdate()
    {
        UpdatePlayerObject();
        if (!_player.inputDeactivated)
        {
            AxisHandler();
        }
        else
        {
            movement = new Vector3(0, 0, 0);
        }
        if (!respawner.IsDead){
	    move();
        try
        {
            visualEffect.Stop();
        }
        catch (System.Exception e)
        {
            Debug.Log(e);
        }
        }
        else{
            try
            {
                visualEffect.Play();
            }
            catch (System.Exception e)
            {
                Debug.Log(e);
            }
        }
    }

    private void Update()
    {
        if (!_player.inputDeactivated)
        {
            ButtonHandler();
        }
    }

    void UpdatePlayerObject()
    {
        PlayerManager playerManager = (PlayerManager)GameObject.FindObjectOfType(typeof(PlayerManager));
        if (playerManager != null)
        {
            _player = playerManager.Players[_PlayerNumber];
        }
        else
        {
            _player = new Player(0, 0);
        }
    }


    void AxisHandler()
    {
        if (_player.GamepadNumber > -1) // player object exists
        {
            string horizontalAxis = _player.Horizontal;
            string verticalAxis = _player.Vertical;

            movement = new Vector3(Input.GetAxisRaw(horizontalAxis), 0, Input.GetAxisRaw(verticalAxis));
            GetComponentInChildren<Animator>().SetFloat("MovementSpeed", movement.magnitude);
        }
    }

    void ButtonHandler()
    {
        if (_player.GamepadNumber > -1) // player object exists
        { 
            string dashKey = _player.Dash;
            isDash = Input.GetKeyDown(dashKey);

            
            string pickUpDropKey = _player.Cancel; //
            // if pickupdropkey pressed
            if (Input.GetKeyDown(pickUpDropKey))
            {
                Debug.Log("Pickup");
                playerInventory.ChangeItem();
            }

            string interact = _player.Interact; //
            if (Input.GetKeyDown(interact)) // if interact key is pressed
            {
                if (playerInventory.item != null)
                {
                    playerInventory.item.UseItem();
                }
                else
                {
                    if (false) //
                    {
                        
                    }
                }
            }
            
        }
    }
    private void move()
    {
        sinceDash += Time.deltaTime;
        Vector3 newPos = body.transform.position;
        if (movement != Vector3.zero){
            body.transform.rotation = Quaternion.RotateTowards(body.transform.rotation, Quaternion.LookRotation(movement, Vector3.up), Time.fixedDeltaTime * 100* turnRate);
            if(body.velocity.y > 0){
                body.velocity = new Vector3(body.velocity.x, 0f, body.velocity.z) ;
            }
            body.velocity = new Vector3(0,body.velocity.y, 0) + movement.normalized * Speed * Time.fixedDeltaTime;
        }
        else{
            body.velocity = new Vector3(0,body.velocity.y,0);
        }
        //Dash Feature ()
        if (isDash && sinceDash > dashCooldown){
            body.velocity += body.transform.forward * DashVelocity * Time.fixedDeltaTime;
            sinceDash = 0f;
         }
    }
}
