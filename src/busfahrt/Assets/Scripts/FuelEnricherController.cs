﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelEnricherController : MonoBehaviour
{
    // 3 different possibilites
    public enum EnricherStatus
    {
        empty,
        cooking,
        finished
    }

    // this should track the current status of the enricher, so that e.g. the player can ask if its ready to take out
    public EnricherStatus Status;
    public GameObject FuelSpot;
    public GameObject EnrichedFuel;

    #region Particles
    private ParticleSystem partSys;
    private ParticleSystem.MainModule partSysMain;
    #endregion


    public float fuel_processing_heat = 100f;
    public float progressScale = 10f;

    public Item fuel = null;
    public float fuel_progress = 0f;
    private BoilerController boiler;

    public float distThreshold = 1.0f;

    private ShipManager ship;

    private void Start()
    {
        boiler = GameObject.Find("Boiler").GetComponent<BoilerController>();
        partSys = GetComponentInChildren<ParticleSystem>();
        partSysMain = partSys.main;
        partSysMain.startColor = new Color(1, 0, 0, 0.5f);
        partSys.Stop();
        ship = GameObject.Find("ShipManager").GetComponent<ShipManager>();

    }

    public void PlaceNewItem(Item newFuel)
    {
        newFuel.transform.position = FuelSpot.transform.position;
        newFuel.transform.SetParent(transform);
        fuel = newFuel;
        Status = EnricherStatus.cooking;

        if (partSys.isPlaying == false)
        {
            partSys.Play();
        }
    }

    public Item giveNewItem()
    {
        // reset the color and stop particle
        partSysMain.startColor = new Color(1, 0, 0, 0.5f);
        partSys.Stop();

        // take the unfinished item out
        if (fuel_progress < 100f)
        {
            Status = EnricherStatus.empty;
            fuel_progress = 0f;
            
        }

        // take the (finished) item
        var f = fuel;
        fuel = null;
        return f;
    }

    private void Update()
    {
        if(Status == EnricherStatus.cooking)// && ship.engineRunning)
        {
            fuel_progress = fuel_progress + progressScale * boiler.heat * Time.deltaTime;
            //item finished
            if (fuel_progress > fuel_processing_heat)
            {
                fuel_progress = 0f;
                Destroy(fuel.gameObject);
                fuel = Instantiate(EnrichedFuel, FuelSpot.transform).GetComponent<Item>();
                Status = EnricherStatus.finished;

                partSys.Stop();
                partSysMain.startColor = new Color(0, 1, 0, 0.5f);
                partSys.Play();
            }

        }

        // The fuel is levitating vertically above the enricher for more clarity, this should not affect the said bug, so i test it horizontally
        if (fuel != null)
        {
            var horizontalDist = Vector2.Distance(new Vector2(fuel.transform.position.x, fuel.transform.position.z),
                                              new Vector2(transform.position.x, transform.position.z));
            if (horizontalDist > distThreshold)
            {
                // Debug.Log("fuel was taken out through bug");
                // fuel was taken out through bug
                fuel = null;
                fuel_progress = 0f;
                Status = EnricherStatus.empty;

                partSysMain.startColor = new Color(1, 0, 0, 0.5f);
                partSys.Stop();
            }
        }
        
    }
}
