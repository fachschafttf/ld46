﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OverlayText : MonoBehaviour
{ 

    public GameObject Text;
    public GameObject TextBox;
    private float sinceStart = 0;
    public int WaiteBeforeShow = 2;
    public int WaitAfterShow = 10;
    public bool deactivateInput = false;
    public bool WaitForInfinity = false;
    private int _wait2 = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (deactivateInput)
        {
            GameObject.Find("PlayerManager").GetComponent<PlayerManager>().inputDeactivated = true;
        }
        if (!WaitForInfinity)
        {
            _wait2 = WaiteBeforeShow + WaitAfterShow;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        sinceStart += Time.fixedDeltaTime;
        if (sinceStart > WaiteBeforeShow)
        {
            if (sinceStart > _wait2 && !WaitForInfinity) {
                if (Text != null) { Text.SetActive(false); }
                gameObject.SetActive(false);
                sinceStart = 0;
                if (Text != null) { Text.GetComponent<TMP_Text>().text = ""; }
                if (TextBox != null)
                {
                    TextBox.GetComponentInChildren<TMP_Text>().text = "";
                    if (GameObject.Find("PlayerManager") != null)
                    {
                        GameObject.Find("PlayerManager").GetComponent<PlayerManager>().inputDeactivated = false;
                    }
                }
                if (deactivateInput)
                {
                    if (GameObject.Find("PlayerManager") != null)
                    {
                        GameObject.Find("PlayerManager").GetComponent<PlayerManager>().inputDeactivated = false;
                    }
                }
                return;
            }
            if (Text != null) { Text.SetActive(true); }
            if (TextBox != null) { TextBox.SetActive(true); }
        }
    }

}
