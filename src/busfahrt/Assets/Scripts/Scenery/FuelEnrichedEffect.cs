﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelEnrichedEffect : MonoBehaviour
{
    public float scrollX = 0.05f;
    public float scrollY = 0.05f;
    private Material material;

    // Start is called before the first frame update
    void Start()
    {
        material = GetComponentInChildren<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        float offysetX = Time.time * scrollX;
        float offsetY = Time.time * scrollY;
        material.mainTextureOffset = new Vector2(offysetX, offsetY);
    }
}
