﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotorRotation : MonoBehaviour
{
    public float rotationSpeed = 1;
    float velocity = 0.0f;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        transform.Rotate(Vector3.up * rotationSpeed);
    }

    public void SetRotationSpeed(float speed)
    {
        float newSpeed = Mathf.SmoothDamp(rotationSpeed, speed, ref velocity, 1f);
        rotationSpeed = speed;
    }
}
