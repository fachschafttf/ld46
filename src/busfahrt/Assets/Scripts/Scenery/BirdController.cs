﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour
{
    private ShipManager shipManager;
    [Tooltip("start sets it to be half the ships max speed.")]
    public float BirdSpeed;
    public float leftScreenBorder;
    public float rightScreenBorder;
    public float SpeedAmplifier;
    public float verticalRandom;
    private float rndVert = 0f;
    private float middleVert;
    // Start is called before the first frame update
    void Start()
    {
        shipManager = GameObject.Find("ShipManager").GetComponent<ShipManager>();
        BirdSpeed = shipManager.maxSpeed * 0.75f;
        SpeedAmplifier = 0.05f;
        Invoke("GetNewRandomValue", 1f);
        middleVert = transform.position.y;
        Invoke("DelayedEnabling", 10f);
        SetAtRightBorder();
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Debug.Log("ShipManagerSpeed: " + shipManager.speed);
        MoveBird();
        BorderCheck();
    }

    void BorderCheck()
    {
        if (transform.position.x < leftScreenBorder)
        {
            SetAtRightBorder();
        }
        if (transform.position.x > rightScreenBorder)
        {
            SetAtLeftBorder();
        }
    }

    void SetAtLeftBorder()
    {
        transform.position = new Vector3(leftScreenBorder, transform.position.y, transform.position.z);
    }

    void SetAtRightBorder()
    {
        transform.position = new Vector3(rightScreenBorder, transform.position.y, transform.position.z);
    }

    void MoveBird()
    {
        var speedDiff = BirdSpeed - shipManager.speed;
        transform.position += new Vector3(- speedDiff * SpeedAmplifier, 0, 0);

        // float step = speed * Time.deltaTime;
        // transform.position = Vector3.MoveTowards(transform.position, target.position, step);

        MoveVertically();
    }

    void DelayedEnabling()
    {
        gameObject.SetActive(true);
    }

    void GetNewRandomValue()
    {
        rndVert = Random.Range(-0.05f, 0.05f);

        // every good game is manipulating rng in some odd fashion:
        if (transform.position.y > middleVert)
        {
            rndVert -= 0.01f;
        }
        else
        {
            rndVert += 0.01f;
        }

        Invoke("GetNewRandomValue", 1f);
    }
    void MoveVertically()
    {
        transform.position += new Vector3(0, rndVert, 0);
    }
}
