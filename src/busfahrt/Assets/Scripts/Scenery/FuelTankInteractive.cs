﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelTankInteractive : MonoBehaviour
{
    public float scaleValue = 0;
    public float maxSize = 26;
    private GameObject scale;
    private GameObject effectLight;
    private Light effectLightSpot;
    private Vector3 offset;
    private Vector3 offsetScale;

    // Start is called before the first frame update
    void Start()
    {
        scale = transform.Find("Scale").gameObject;
        effectLight = transform.Find("EffectLight").gameObject;
        effectLightSpot = effectLight.GetComponent<Light>();
        offset = scale.transform.localPosition;
        offsetScale = scale.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void setScale(float scaleValue)
    {
        scaleValue = Mathf.Min(100, Mathf.Max(scaleValue, 0));
        this.scaleValue = scaleValue;
        scale.transform.localPosition = new Vector3(-(scaleValue / maxSize) / 2, 0, 0) + offset;
        scale.transform.localScale = new Vector3(scaleValue / maxSize, offsetScale.y, offsetScale.z);

        effectLight.transform.localPosition = new Vector3(-(scaleValue / maxSize) / 2, 1 + scaleValue / 50, 0) + offset;

        effectLightSpot.range = (scaleValue / maxSize) * 3;
        effectLightSpot.intensity = (scaleValue / maxSize) * 2;
    }
}
