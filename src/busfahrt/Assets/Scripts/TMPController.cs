﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TMPController : MonoBehaviour
{
    public BoilerController boiler;
    public SteamEngineInteractive steamEngine;
    public FuelTankInteractive fuelTank;
    public ProgressController progressController;
    public float pressure = 1;
    public float speed;
    public float fuelScale = 0;
    public float progress = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (boiler != null)
            // boiler.setPressure(this.pressure);
        if (steamEngine != null)
            steamEngine.setPressure(speed);
        if (fuelTank != null)
            fuelTank.setScale(fuelScale);
        if (progressController != null)
            progressController.zeppelinProgress = progress;
    }
}
