﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    public IEnumerator Shake(float duration, float magnitude)
    {
        Vector3 originalPos = transform.localPosition;
        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            var x = Random.Range(-1f, 1f) * magnitude;
            var z = Random.Range(-1f, 1f) * magnitude;
            
            transform.localPosition = new Vector3(x, originalPos.y, z);
            elapsed += Time.deltaTime;

            yield return null;
        }

        transform.localPosition = originalPos;
    }

    public void SmallShake()
    {
        StartCoroutine(Shake(.2f, .1f));
    }

    public void StrongShake()
    {
        StartCoroutine(Shake(.3f, .4f));
    }

}
