﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.InteropServices;
using System.Security.Principal;
using UnityEngine;


public struct Player
{
    public Player(int playerNumber, int gamepadNumber)
    {
        _gamepadNumber = gamepadNumber; // 0 is keyboard/mouse, 1-16 are Gampads
        PlayerNumber = playerNumber;
        Horizontal = "Horizontal";
        Vertical = "Vertical";
        Dash = "space";
        Interact = "e";
        Cancel = "q";
        Spawnpoint = new Vector3(0,1,0);
        inputDeactivated = false;
        _assignKeys(gamepadNumber);
    }

    private int _gamepadNumber;
    public int GamepadNumber {
        get { return _gamepadNumber; }
        set {
            _assignKeys(value);
            _gamepadNumber = value;
        } }
    public int PlayerNumber { get; set; }
    public string Horizontal { get; set; }
    public string Vertical { get; set; }
    public string Dash { get; set; }
    public string Interact { get; set; }
    public string Cancel { get; set; }
    public Vector3 Spawnpoint { get; set; }

    public bool inputDeactivated;

    private void _assignKeys(int gamepadNumber)
    {
        if (gamepadNumber == 0) // keyboard
        {
            Horizontal = "Horizontal";
            Vertical = "Vertical";
            Dash = "space";
            Interact = "e";
            Cancel = "f";
        }
        else
        {
            Horizontal = "HorizontalJoy" + gamepadNumber;
            Vertical = "VerticalJoy" + gamepadNumber;
            Dash = "joystick " + gamepadNumber + " button " + 2;
            Interact = "joystick " + gamepadNumber + " button " + 0;
            Cancel = "joystick " + gamepadNumber + " button " + 1;
        }
    }

    public override string ToString() => $"(Player {PlayerNumber}, with Gamepad {GamepadNumber})";
}
public class PlayerManager : MonoBehaviour
{
    public Player[] Players;
    public const int MaxPlayers = 4;
    private PlayerManager instance;
    private bool _inputDeactivated = false;
    public bool inputDeactivated {
        get
        {
            return _inputDeactivated;
        }
        set 
        {
            for (int i = 0; i < Players.Length; i++)
            {
                Players[i].inputDeactivated = value;
            }
            _inputDeactivated = value;
        }
   }

    // Start is called before the first frame update
    void Awake()
    {
        // Keep same Audiomanager between sceneswitches
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        Players = new Player[MaxPlayers];

        for (int i = 0; i < MaxPlayers; i++)
        {
            Players[i] = new Player(i, -1);
        }
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int PlayerCount
    {
        get
        {
            int count = 0;
            foreach (Player player in Players)
            {
                count += player.GamepadNumber > -1 ? 1 : 0;
            }
            return count;
        }
    }
}
