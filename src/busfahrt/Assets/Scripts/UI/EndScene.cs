﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScene : MonoBehaviour
{
    private Scene startedScene;
    public bool disapleUserInput = true;
    // Start is called before the first frame update
    void Start()
    {
        startedScene = SceneManager.GetActiveScene();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnEnable()
    {
        PlayerManager[] players = Object.FindObjectsOfType<PlayerManager>();
        foreach(PlayerManager player in players)
        {
            if (disapleUserInput)
                player.inputDeactivated = true;
        }
    }

    public void Replay()
    {
        PlayerManager[] players = Object.FindObjectsOfType<PlayerManager>();
        foreach (PlayerManager player in players)
        {
            if (disapleUserInput)
                player.inputDeactivated = false;
        }
        SceneManager.LoadScene(startedScene.buildIndex, LoadSceneMode.Single);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void Back()
    {
        
        PlayerManager[] players = Object.FindObjectsOfType<PlayerManager>();
        foreach (PlayerManager player in players)
        {
            if (disapleUserInput)
                player.inputDeactivated = false;
        }
        gameObject.SetActive(false);
    }
}
