﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelControll : MonoBehaviour
{
    public Transform fuelSpawnCenter;
    public float x_deviation;
    public float z_deviation;

    public float spawnrate = 0.1f;
    public float spawnOffset = 0.1f;
    private float spawnthreshold = 1f;
    public float spawntimer = 1.0f;

    public GameObject fuel;

    public float fillLevel = 50;
    public float fillMax = 100;
    public float consumption = 10;
    public float fuelPerStack = 25;

    private ShipManager ship;

    // Start is called before the first frame update
    void Start()
    {
        spawnFuel();
        ship = GameObject.Find("ShipManager").GetComponent<ShipManager>();
    }

    // Update is called once per frame
    void Update()
    {
        spawntimer = spawntimer - spawnrate * Time.deltaTime;
        if(spawntimer < 0f)
        {
            // Debug.Log("do I reach this");
            spawntimer = spawnthreshold + Random.Range(-spawnOffset, spawnOffset);
            spawnFuel();
        }

        fillLevel = fillLevel - consumption * ship.speed * Time.deltaTime;
        UpdateFillLevel();
    }

    public void spawnFuel()
    {
        // Debug.Log("does this happen");
        float x_offset = Random.Range(-x_deviation, x_deviation);
        float z_offset = Random.Range(-z_deviation, z_deviation);
        Vector3 spawnPosition = fuelSpawnCenter.position + new Vector3(x_offset, 0f, z_offset);
        Instantiate(fuel, spawnPosition, Quaternion.identity);
    }

    public void insertFuel()
    {
        // Debug.Log("Fuel Inserted");
        fillLevel = fillLevel + fuelPerStack;
        if(fillLevel > fillMax)
        {
            fillLevel = fillMax;
        }
        UpdateFillLevel();
    }

    private void UpdateFillLevel()
    {
        if(fillLevel < 0)
        {
            fillLevel = 0;
            ship.EngineBreackdown();
        }
        gameObject.GetComponent<FuelTankInteractive>().setScale(fillLevel);
    }
}
