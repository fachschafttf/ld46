﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this script was created to test the camera shake
public class Explosion : MonoBehaviour
{
    public CameraShaker CameraShaker;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CameraShaker.SmallShake();
        }
        else
        {
            if (Input.GetMouseButtonDown(1))
            {
                CameraShaker.StrongShake();
            }
        }

    }
}
