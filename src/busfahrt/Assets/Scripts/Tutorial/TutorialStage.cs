﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class TutorialStage : MonoBehaviour
{
    // Start is called before the first frame update
    public string HelpText;
    public HelpTextTypeEnum HelpTextType;
    public GameObject[] TutorialObjects;
    public int priority;
    public string successComponent;
    public string successVariableName;
    public float successVariableValue;
    public bool sucess = false;

    public enum HelpTextTypeEnum
    {
        Textbox,
        OverlayText
    }

    void Start()
    {
        foreach (GameObject obj in TutorialObjects)
        {
            obj.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
