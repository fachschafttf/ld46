﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using TMPro;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public GameObject TextBox;
    public GameObject OverlayText;
    public GameObject CanvasOverlayText;
    public GameObject CanvasTextBox;
    public GameObject Fader;
    public string[] StageHelpText;
    public string WelcomeText;
    public string FinishText;
    private bool closeOnInput = false;
    private bool _tutorialFinish = false;
    private int currentStage = -1;
    private bool[] successStage;
    private bool stageChanged = false;
    public GameObject Wheel;
    public GameObject LogPile;
    public GameObject FuelHatch;
    public GameObject FuelHatchButton;
    public GameObject BoilerPressurePlate;

    // Start is called before the first frame update
    void Start()
    {
        ShowOverLayText("This is your ship, try to keep it alive", 2, 4);
        ShowTextBox(WelcomeText, 10, 0, false);
        successStage = new bool[StageHelpText.Length];
        for (int i = 0; i < successStage.Length; i++)
        {
            successStage[i] = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (closeOnInput)
        {
            if (Input.GetKeyDown(KeyCode.Space) ||
                Input.GetKeyDown("joystick " + "button " + 0))
            {
                closeOnInput = false;
                CanvasTextBox.GetComponent<OverlayText>().WaitAfterShow = 0;
                CanvasTextBox.GetComponent<OverlayText>().WaitForInfinity = false;
                if (currentStage == -1)
                {
                    nextStage();
                }
                if (currentStage >= 4)
                {
                    successStage[currentStage] = true;
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (_tutorialFinish)
        {
            Fader.GetComponent<FadeInOut>().Direction = FadeInOut.FadeDirection.In;
            Fader.GetComponent<FadeInOut>().SceneToLoad = "MainMenu";
            Fader.SetActive(true);
        }
        if (stageChanged)
        {
            Debug.Log(currentStage);
            ShowTextBox(StageHelpText[currentStage], 0, 0);
            stageChanged = false;
        }
        if (currentStage == 0 && Wheel.GetComponent<WheelController>().rotation_value > 1)
        {
            successStage[currentStage] = true;
            LogPile.SetActive(true);
        }
        else if (currentStage == 1 && FuelHatch.GetComponent<FuelHatch>().isLog)
        {
            successStage[currentStage] = true;
        }
        else if (currentStage == 2 && FuelHatchButton.GetComponent<FuelHatchButton>().buttonPressed)
        {
            successStage[currentStage] = true;
        }
        else if (currentStage == 3 && BoilerPressurePlate.GetComponent<SteamValveButton>().buttonPressed)
        {
            successStage[currentStage] = true;
        }
        if (currentStage > -1)
        {
            if (currentStage >= StageHelpText.Length || (successStage[currentStage] && currentStage == StageHelpText.Length - 1))
            {
                _tutorialFinish = true;
            }
            else if (successStage[currentStage])
            {
                nextStage();
            }
        }
    }


    void ShowOverLayText(string text, int showAfter = 0, int showSeconds = 2)
    {
        OverlayText.GetComponent<TMP_Text>().text = text;
        CanvasOverlayText.GetComponent<OverlayText>().WaiteBeforeShow = showAfter;
        CanvasOverlayText.GetComponent<OverlayText>().WaitAfterShow = showSeconds;
        CanvasOverlayText.GetComponent<OverlayText>().WaitForInfinity = false;
        CanvasOverlayText.SetActive(true);
    }

    void ShowTextBox(string text, int showAfter = 0, int showSeconds = 20, bool deactivateInputs = true)
    {
        TextBox.transform.Find("Text").GetComponent<TMP_Text>().text = text;
        CanvasTextBox.GetComponent<OverlayText>().WaiteBeforeShow = showAfter;
        CanvasTextBox.GetComponent<OverlayText>().WaitAfterShow = showSeconds;
        CanvasTextBox.GetComponent<OverlayText>().WaitForInfinity = true;
        CanvasTextBox.GetComponent<OverlayText>().deactivateInput= deactivateInputs;
        CanvasTextBox.SetActive(true);
        closeOnInput = true;
    }

    private void nextStage() {
        currentStage += 1;
        stageChanged = true;
    }
}
