﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelHatchButton : MonoBehaviour
{
    public GameObject hatch;
    public bool buttonPressed;
    private FuelHatch fuelHatch;
    private FuelHatchDecorator fuelHatchDecorator;
    private Animator leverAnimator;
    private ShipManager ship;
    // Start is called before the first frame update
    void Start()
    {
        fuelHatch = hatch.GetComponent<FuelHatch>();
        fuelHatchDecorator = hatch.GetComponent<FuelHatchDecorator>();
        ship = GameObject.Find("ShipManager").GetComponent<ShipManager>();
        leverAnimator = GetComponentInChildren<Animator>();
        buttonPressed = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            ButtonPressed();
            buttonPressed = true;
        }
    }

    private void ButtonPressed()
    {
        Debug.Log("button pressed");
        Debug.Log(fuelHatchDecorator);
        //if (!ship.engineRunning)
        //{
        //    return;
        //}
        if(fuelHatch != null)
            fuelHatch.ButtonPressed();
        if (fuelHatchDecorator != null)
            fuelHatchDecorator.ButtonPressed();
        if (leverAnimator)
            leverAnimator.SetTrigger("Toggle");
    }
}
