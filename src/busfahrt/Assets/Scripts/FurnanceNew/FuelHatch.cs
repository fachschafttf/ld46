﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.Experimental.VFX;

public class FuelHatch : MonoBehaviour
{
    private LayerMask mask_items;

    private bool buttonInProgress = false;
    private int log_counter = 0;
    private Vector3 outPos;
    private Vector3 inPos;

    public bool movein = false;
    public bool moveout = false;

    private Vector3 velocity = Vector3.zero;
    public float smoothTime = 2f;

    private List<GameObject> logs = new List<GameObject>();

    public VisualEffect fire1;
    public GameObject furnace;
    public float fuelPerLog = 100f;

    public bool isLog = false;
    // Start is called before the first frame update
    void Start()
    {
        mask_items = LayerMask.GetMask("Item");
        outPos = transform.position;
        inPos = new Vector3(transform.position.x, transform.position.y, -1f);
        fire1.Stop();
        //furnace = GameObject.Find("Furnace");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (movein)
        {
            transform.position = Vector3.SmoothDamp(transform.position, inPos, ref velocity, smoothTime);
            if(Vector3.Distance(transform.position, inPos) < 0.1)
            {
                MovedIn();
            }
        }
        else if (moveout)
        {
            transform.position = Vector3.SmoothDamp(transform.position, outPos, ref velocity, smoothTime);
            if (Vector3.Distance(transform.position, outPos) < 0.1)
            {
                MovedOut();
            }
        }
        //is a log on plate
        Collider[] items = Physics.OverlapBox(transform.position + transform.up, (transform.localScale + new Vector3(0f, 2f, 0f)) / 2, Quaternion.identity, mask_items);
        isLog = false;
        foreach (Collider col in items)
        {
            if (col.gameObject.tag == "Log" && col.gameObject.transform.position.y < 0.4)
            {
                isLog = true;
                break;
            }
        }
    }

    private void MovedIn()
    {
        movein = false;
        moveout = true;
        LogsInFurnance();
    }

    IEnumerator stopFire(VisualEffect fire)
    {
        yield return new WaitForSeconds(3.0f);
        fire.Stop();
    }
    private void MovedOut()
    {
        moveout = false;
        movein = false;
        buttonInProgress = false;
    }

    public void ButtonPressed()
    {
        if (buttonInProgress)
        {
            return;
        }
        else
        {
            buttonInProgress = true;
            logs = new List<GameObject>();
        }
        log_counter = 0;
        Collider[] items = Physics.OverlapBox(transform.position + transform.up, (transform.localScale + new Vector3 (0f,2f,0f)) /2, Quaternion.identity, mask_items);

        foreach(Collider col in items)
        {
            if(col.gameObject.tag == "Log" && col.gameObject.transform.position.y < 0.4)
            {
                log_counter = log_counter + 1;
                logs.Add(col.gameObject);
                col.gameObject.transform.SetParent(transform);
            }
        }
        Debug.Log(log_counter);
        //start Movein
        movein = true;
    }

    private void LogsInFurnance()
    {
        Debug.Log("Burn");
        foreach(GameObject obj in logs)
        {
            Object.Destroy(obj);
            //TODO:
            //call furnace/Boiler with log
            //Fire effect
        }
        if(log_counter > 0)
        {
            if (fire1) {
                fire1.Play();
                StartCoroutine(stopFire(fire1));
            }
            furnace.GetComponent<FurnaceController>().AddFuel(fuelPerLog * log_counter);
            
        }
        log_counter = 0;
    }

    public bool CanPress()
    {
        return !buttonInProgress;
    }
}
