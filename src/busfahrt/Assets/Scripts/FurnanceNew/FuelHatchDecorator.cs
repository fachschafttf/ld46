﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelHatchDecorator : MonoBehaviour
{
    private GameObject hatch;
    private FuelHatch fuelHatch;
    private Animator animator;

    void Start()
    {
        hatch = transform.Find("FuelHatch").gameObject;
        fuelHatch = hatch.GetComponent<FuelHatch>();
        animator = GetComponentInChildren<Animator>();
    }


    public void ButtonPressed()
    {
        Debug.Log("Pressed");
        if (fuelHatch.CanPress())
        {
            animator.SetTrigger("StartGrab");
        }
    }

    public void StartHatch()
    {

        Debug.Log("Start Hatch");
        fuelHatch.ButtonPressed();
    }

}
