﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class RespawnController : MonoBehaviour
{
    public Vector3 Spawnpoint {get; set;}
    public bool IsDead { get => isDead; set => isDead = value; }

    private float sinceDeath;
    public float respawnCooldown;

    private bool isDead;
    private Rigidbody body;     
    void Start()
    {
         body = GetComponent<Rigidbody> ();
        if (TryGetComponent<Player>(out Player player )){
            Spawnpoint = player.Spawnpoint;
        }
        else{
            Spawnpoint = new Vector3(0,1f,0);
        }
        
    }
    void FixedUpdate()
    {
        checkNeedRespawn();
    }

    private void checkNeedRespawn(){
        if(body.transform.position.y < -0.3f){
            IsDead = true;
        }
        if(IsDead)
        {
            sinceDeath += Time.fixedDeltaTime;
        }

        if (sinceDeath > respawnCooldown){
            body.transform.rotation = Quaternion.LookRotation(Vector3.zero, Vector3.up);
            body.transform.position = Spawnpoint;
            sinceDeath = 0f;
            IsDead = false;      

        }
    }
}
