﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTracker : MonoBehaviour
{
    public float rotation_value = 0;
    public float rotation_scaling = 10f;
    private float last_rotation;
    // Start is called before the first frame update
    void Start()
    {
        last_rotation = transform.rotation.y;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        float delta_rot = last_rotation - transform.rotation.y;
        rotation_value = rotation_value - rotation_scaling * delta_rot * Time.deltaTime;
        last_rotation = transform.rotation.y;
    }
}
