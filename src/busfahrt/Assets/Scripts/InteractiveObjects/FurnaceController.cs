﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnaceController : MonoBehaviour {
    public float fuel_consumption = 0.1f;
    public BoilerController boiler;

    public GameObject[] LogSpot = new GameObject[3];

    public float PlayerDetectionRadius;
    public LayerMask PlayerLayer;

    private Item[] SpotLog = { null, null, null };

    private float fuel = 0f;

    void Start()
    {
    }

    void Update()
    {

    }

    void FixedUpdate() {
        BurnFuel();
    }
    
    public void AddFuel(float fuel_value) {
        fuel += fuel_value;
    }

    public void Test()
    {
        Debug.Log("function can be called");
    }

    void BurnFuel() {
        if (fuel > 0) {
            fuel -= boiler.AddHeat(fuel < fuel_consumption ? fuel : fuel_consumption);
        }
    }
}
