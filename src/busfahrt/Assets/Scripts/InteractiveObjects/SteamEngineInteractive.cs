﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamEngineInteractive : MonoBehaviour
{
    private float pressure;
    private BarometerInteravtive barometer;
    private ParticleSystem.MainModule steamSystemMain;
    // Start is called before the first frame update
    void Start()
    {
        barometer = GetComponentInChildren<BarometerInteravtive>();
        steamSystemMain = GetComponentInChildren<ParticleSystem>().main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setPressure(float pressure)
    {
        GetComponentInChildren<Animator>().speed = pressure/10;
        barometer.pressure = pressure;
        steamSystemMain.startSpeed = 0.5f + Mathf.Pow(pressure, 1.2f) * 0.05f;
        steamSystemMain.startSize = 0.5f + Mathf.Pow(pressure, 1.2f) * 0.01f;
        steamSystemMain.startLifetime = 0.3f + Mathf.Pow(pressure, 1.2f) * 0.0005f;
    }
}
