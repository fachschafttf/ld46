﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoilerController : MonoBehaviour
{
    public float steamHeatLoss = 10f;
    public float refinerHeatLoss = 10f;
    public int refinerFactor = 0;


    public float fuel_heat_fraction = 0.002f;
    public float overheat_threshold = 100f;
    public float overheat_cooldown = 10f;
    public float heat_dissipation_rate = 0.001f;
    public FuelEnricherController[] FuelTransformer = new FuelEnricherController[3];
    public FireStarter firestarter;
    public HUDBar hudbar;
    public CameraShaker camerashaker;
    public LayerMask playerLayer;

    public bool overheated = false;
    public float current_overheat_cooldown = 0f;

    public float _heat = 0f;

    private Renderer heatPipeRender1;
    private Renderer heatPipeRender2;
    private Material[] temperatureMaterials = new Material[5];

    private ShipManager ship;

    public float heat {
        get { return _heat; }

        private set { _heat = value;  if(hudbar) hudbar.SetValue(value);}

    }

    public float heatPercentage {
        get
        {
            return heat / overheat_threshold;
        }
    }
    

    private Item[] SpotFuel = { null, null, null };
    //private ParticleSystem steamSystem;
    //private ParticleSystem.MainModule steamSystemMain;

    public float AddHeat(float amount) {
        if (heat + amount < overheat_threshold) {
            heat += amount;
            SetHeatColor(heat);
            return amount;
        } else {
            float h = overheat_threshold - heat;
            heat = overheat_threshold;
            Overheat();
            return h;
        }
    }

    public void extinguish() {
        heat = Mathf.Max(0f, heat - 30f);
        overheated = false;
        current_overheat_cooldown = overheat_cooldown;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        if (hudbar) {
            hudbar.SetMaxValue(overheat_threshold);
        }
        heat = 0f;
        //steamSystem = GetComponentInChildren<ParticleSystem>();
        //steamSystemMain = steamSystem.main;
        //barometer = transform.Find("Barometer").GetComponent<BarometerInteravtive>();
        temperatureMaterials[0] = (Material)Resources.Load("ColorPaletteMetallic", typeof(Material));
        temperatureMaterials[1] = (Material)Resources.Load("Temperature/Cold", typeof(Material));
        temperatureMaterials[2] = (Material)Resources.Load("Temperature/Warm", typeof(Material));
        temperatureMaterials[3] = (Material)Resources.Load("Temperature/Warmer", typeof(Material));
        temperatureMaterials[4] = (Material)Resources.Load("Temperature/Hot", typeof(Material));
        heatPipeRender1 = transform.Find("Boiler/TemperaturePipe1").GetComponent<MeshRenderer>();
        heatPipeRender2 = transform.Find("Boiler/TemperaturePipe2").GetComponent<MeshRenderer>();
        ship = GameObject.Find("ShipManager").GetComponent<ShipManager>();
    }

    private void Update()
    {
        if (overheated) return;
        if (current_overheat_cooldown > 0f) current_overheat_cooldown -= Time.deltaTime;
        refinerFactor = 0;
        for (int i = 0; i < 3; ++i)
        {
            if(FuelTransformer[i].Status == FuelEnricherController.EnricherStatus.cooking)
            {
                refinerFactor = refinerFactor + 1;
            }
        }
        //if (!ship.engineRunning)
        //{
        //    refinerFactor = 0;
        //}
        //update heat
        heat = heat - (steamHeatLoss + (refinerHeatLoss * refinerFactor)) * Time.deltaTime;
        if(heat < 0)
        {
            heat = 0f;
        }

    }

    // Update is called once per frame
    void FixedUpdate() {
        //if (overheated) return;
        ////heat *= (1f - heat_dissipation_rate);
        //var fuel_transformer_heat = heat * fuel_heat_fraction;
        //for (int i = 0; i < 3; ++i) {
        //    heat -= FuelTransformer[i].AddHeat(fuel_transformer_heat);
        //}
        SetHeatColor(heat);
    }

    private void Overheat() {
        // only perform overheating effects when the heat rises over the overheat threshold
        if (overheated == true) return;
        // don't overheat if recently extinguished
        if (current_overheat_cooldown > 0f) return;
        overheated = true;
        camerashaker.SmallShake();

        // move players out of the way
        Vector3 explosion_center = transform.position - transform.forward;
        float explosion_radius = 5f;
        Collider[] hit_players = Physics.OverlapSphere(explosion_center, explosion_radius, playerLayer);
        foreach (Collider player in hit_players) {
            if (player.gameObject.tag == "Player") {
                var body = player.GetComponent<Rigidbody>();
                Vector3 difference = body.transform.position - explosion_center;
                Vector3 newPos = explosion_center + difference.normalized * explosion_radius;
                newPos.y = 0f;
                body.transform.position = newPos;
                body.velocity = difference.normalized;
            }
        }

        // create fires
        int number_of_fires = 3;
        Vector3[] fire_positions = new Vector3[number_of_fires];
        for (int i = 0; i < number_of_fires; ++i) {
            fire_positions[i] = explosion_center +
                Quaternion.Euler(0, Random.Range(-45.0f, 45.0f), 0f) * transform.forward * Random.Range(-explosion_radius + 1f, -1.5f);
        }
        //Vector3 fire_position = transform.position - 2.5f * transform.forward;
        foreach (Vector3 fire_position in fire_positions) {
            Collider[] hit_fires = Physics.OverlapSphere(fire_position, 0.4f);
            bool isfire = false;
            foreach (Collider fire in hit_fires) {
                if (fire.gameObject.tag == "Flame") {
                    isfire = true;//do nothing
                }
            }
            if (!isfire) {
                firestarter.GetComponent<FireStarter>().StartFire(fire_position - new Vector3(0, 1.8f, 0));
            }
        }
    }


    private void SetHeatColor(float heat)
    {
        for (int i = 1; i < 5; i++)
        {
            if (heat < i * 25)
            {
                heatPipeRender1.materials = new Material[] { temperatureMaterials[i], temperatureMaterials[0] };
                heatPipeRender2.materials = new Material[] { temperatureMaterials[i], temperatureMaterials[0] };
                break;
            }
        }
    }
}
