﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarometerInteravtive : MonoBehaviour
{
    private float _pressure = 0f;
    public float max_pressure = 100f;
    public float pressure {
        get { return _pressure; }
        set { _pressure = value;
            needle.transform.localRotation = Quaternion.Euler(0, (value / max_pressure) * 270, 0);
        }
    }
    private GameObject needle;
    // Start is called before the first frame update
    void Start()
    {
        needle = transform.Find("Needle").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
