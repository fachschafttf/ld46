﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public enum ItemTypes
    {
        log,
        bucket,
        fuelUnprocessed,
        fuelFin
    }

    public ItemTypes ItemType;

    public void SetPosition(Vector3 targetPosition)
    {
        transform.position = targetPosition;
    }

    public void UseItem()
    {

        switch (ItemType)
        {
            case ItemTypes.bucket:
                GetComponent<BucketController>().UseItem();
                break;
            case ItemTypes.fuelUnprocessed:
                GetComponent<FuelUController>().UseItem();
                break;
            case ItemTypes.fuelFin:
                GetComponent<FuelFController>().UseItem();
                break;
            default:
                break;
        }
    }
}
