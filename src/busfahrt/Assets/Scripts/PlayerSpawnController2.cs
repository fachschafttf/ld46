﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSpawnController2 : MonoBehaviour
{
    public GameObject playerPrefab;
    public PlayerManager playerManagerPrefab;
    private PlayerManager _playerManager;


    // Start is called before the first frame update
    void Start()
    {
        foreach (string name in Input.GetJoystickNames())
        {
            Debug.Log("GamePad initialized: " + name);
        }
        _playerManager = (PlayerManager)GameObject.FindObjectOfType(typeof(PlayerManager));
        if (_playerManager == null)
        {
            _playerManager = Instantiate(playerManagerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    // called first
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (_playerManager != null)
        {
            for (int i = 0; i < _playerManager.Players.Length; i++)
            {
                Player player = _playerManager.Players[i];
                if (player.GamepadNumber > -1)
                {
                    Vector3 spawnpoint = GameObject.Find("SpawnPoint" + (player.PlayerNumber + 1)).transform.position;
                    _playerManager.Players[i].Spawnpoint = spawnpoint;
                    GameObject newPlayerObject = Instantiate(playerPrefab, spawnpoint, Quaternion.identity);
                    newPlayerObject.GetComponent<PlayerController>().PlayerNumber = player.PlayerNumber;
                }
            }
        }
    }


}
